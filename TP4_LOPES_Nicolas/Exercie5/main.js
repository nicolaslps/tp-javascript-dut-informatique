



function verification(chaine){
    if(startWithAToD(chaine)){
        document.getElementById("startA_B").style.backgroundColor   = "#8FE865"; 
    }else{
        document.getElementById("startA_B").style.backgroundColor   = "#ffe5e5"; 
    }


    if(haveArobase(chaine)){
        document.getElementById("arobase").style.backgroundColor   = "#8FE865"; 
    }else{
        document.getElementById("arobase").style.backgroundColor   = "#ffe5e5"; 
    }
    
    
    if(haveOneNumberOrMore(chaine)){
        document.getElementById("one_number_or_more").style.backgroundColor   = "#8FE865"; 
    }else{
        document.getElementById("one_number_or_more").style.backgroundColor   = "#ffe5e5"; 
    }


}

function startWithAToD(chaine){
    if (chaine.match(/^[a-dA-D]/i)) {
        return true;
    }else{
        return false;
    }
}


function haveArobase(chaine){
    if (chaine.match(/@{1,1}/gi)) {
        return true;
    }else{
        return false;
    }
}

function haveOneNumberOrMore(chaine){
    if (chaine.match(/[0-9]+/i)) {
        return true;
    }else{
        return false;
    }
}

function remplacerlesnombres(chaine){
    document.getElementById("nombres_rempalces").innerHTML = "la chaines avec les chiffres en * = "+chaine.replace(/\d/gi, "*");
}