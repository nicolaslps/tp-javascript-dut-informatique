var resultat = "";
var calcul = "";
var inputs = document.getElementsByTagName("input"); 

function updateResult(value){
    resultat += value; 
    calcul += value; 
    document.getElementById("resultat").value = resultat;
    changeStateOperator(false);
    changeStateDot(false);
}

function updateResultWithOperator(value){
    resultat += value;
    calcul += value;

    if (resultat.charAt(resultat.length-1) == "*") {
        resultat = resultat.replace(resultat.charAt(resultat.length-1),"×");
    }  
    if (resultat.charAt(resultat.length-1) == "/") {
        resultat = resultat.replace(resultat.charAt(resultat.length-1),"÷");
    }  
    document.getElementById("resultat").value = resultat;
    changeStateOperator(true);
}

function updateResultWithDot(value){
    resultat += value;
    calcul += value;
    
    document.getElementById("resultat").value = resultat;
    changeStateDot(true);
    changeStateOperator(true);
}

function showResult(){ 


    if (document.getElementById("resultat").value == "" && calcul == "") {
        document.getElementById("resultat").value = 0;
    } else {
        if (calcul == "") {
            resultat = document.getElementById("resultat").value;
            document.getElementById("resultat").value = eval(resultat);
        }else{
            if (calcul.charAt(0) == "*" || calcul.charAt(0) == "/") {
                calcul = calcul.replace(calcul.charAt(0),"");
                alert("expression mal formée mais l'opérateur au début du calcul a été supprimé automatiquement");
            }
            if (calcul.charAt(calcul.length-1) == "*" || calcul.charAt(calcul.length-1) == "/" || calcul.charAt(calcul.length-1) == "-" || calcul.charAt(calcul.length-1) == "+") {
                calcul = calcul.replace(calcul.charAt(resultat.length-1)," ");
                alert("expression mal formée mais l'opérateur a la fin du calcul a été supprimé automatiquement");
            }
            document.getElementById("resultat").value = eval(calcul);
        }        
    }
    resultat = ""; 
    calcul = ""; 
}

function resetResult(){
    resultat = "";
    calcul = "";
    document.getElementById("resultat").value = resultat;
}

function changeStateOperator(action){
    inputs[4].disabled = action;
    inputs[8].disabled = action;
    inputs[12].disabled = action;
    inputs[15].disabled = action;
}

function changeStateDot(action){
    inputs[14].disabled = action; 
}

function verifCaracteres(event){ 
	var touche = String.fromCharCode(event.keyCode);
	var champ = document.getElementById('resultat');
	var caracteres = '()/*-+0123456789';
    if(caracteres.indexOf(touche) >= 1) {
        champ.value += touche;
    }else{
        alert("Les caractères alphabétique sont interdits");
    }
}
