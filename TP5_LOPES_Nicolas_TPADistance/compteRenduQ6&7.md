# Compte rendu Javascript 



## Exercice 6

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Exercice 7</title>
    <meta charset="utf-8" />
  </head>
  <body>
    <div>
      <div>
        <span id="h">0 h</span> : <span id="min">0 min</span> : <span id="s">0 s</span> : <span id="ms">0 ms</span>
      </div>
      <button id="start" onclick="startTimer()">Start</button>
      <button id="stop" onclick="stopTimer()">Stop</button>
      <button id="reset" onclick="resetTimer()">Reset</button>
    </div>

    <script src="./main.js"></script>
  </body>
</html>

```


```javascript
 var spanList = document.getElementsByTagName("span");
var startButton = document.getElementById("start");
var stopButton = document.getElementById("stop");
var t;
var milliseconde = 0;
var seconde = 0;
var minutes= 0;
var heure = 0;

function startTimer() {
  t = setInterval(update, 100);
  startButton.disabled = true;
}

function resetTimer() {
  clearInterval(t);
  startButton.disabled = false;
  (milliseconde = 0), (seconde = 0), (minutes= 0), (heure = 0);
  spanList[0].innerHTML=heure+" h";
  spanList[1].innerHTML=minutes+" min";
  spanList[2].innerHTML=seconde+" s";
  spanList[3].innerHTML=milliseconde+" ms"; 
}

function stopTimer() {
  clearInterval(t);
  startButton.disabled = false;
}

function update() {
  milliseconde += 1;
  if (milliseconde == 10) {
    milliseconde = 1;
    seconde += 1;
  }
  if (seconde == 60) {
    seconde = 0;
    minutes+= 1;
  }
  if (minutes== 60) {
    minutes= 0;
    heure += 1;
  }
  spanList[0].innerHTML=heure+" h";
  spanList[1].innerHTML=minutes+" min";
  spanList[2].innerHTML=seconde+" s";
  spanList[3].innerHTML=milliseconde+" ms"; 
}
```


## Exercice 7

```html
<!doctype html>
<html lang="en">
    <title>Calculatrice</title>
</head>
<body>
  <form action="" id="myForm">
    <label>Résultat: </label><br>
    <input type="text" name="texte" id="resultat" onkeypress="verifCaracteres(event); return false;"/> <br>
  
    <input type="button" value="1" onclick="updateResult('1')" />
    <input type="button" value="2" onclick="updateResult('2')" />
    <input type="button" value="3" onclick="updateResult('3')" />
    <input type="button" value="*" onclick="updateResultWithOperator('*')" /> <br>
    <input type="button" value="4" onclick="updateResult('4')" />
    <input type="button" value="5" onclick="updateResult('5')" />
    <input type="button" value="6" onclick="updateResult('6')" />
    <input type="button" value="+" onclick="updateResultWithOperator('+')" /> <br>
    <input type="button" value="7" onclick="updateResult('7')" />
    <input type="button" value="8" onclick="updateResult('8')" />
    <input type="button" value="9" onclick="updateResult('9')" />
    <input type="button" value="-" onclick="updateResultWithOperator('-')" /> <br>
    <input type="button" value="0" onclick="updateResult('0')" /> 
    <input type="button" value="." onclick="updateResultWithDot('.')" />
    <input type="button" value="/" onclick="updateResultWithOperator('/')" /> <br>
    <input type="button" value="=" onclick="showResult()" /> 
    <input type="button" value="AC" onclick="resetResult()" /> <br>
  </form>
    <script src="./main.js"></script>
  </body>
</html>

```


```javascript
var resultat = "";
var calcul = "";
var inputs = document.getElementsByTagName("input"); 

function updateResult(value){
    resultat += value; 
    calcul += value; 
    document.getElementById("resultat").value = resultat;
    changeStateOperator(false);
    changeStateDot(false);
}

function updateResultWithOperator(value){
    resultat += value;
    calcul += value;

    if (resultat.charAt(resultat.length-1) == "*") {
        resultat = resultat.replace(resultat.charAt(resultat.length-1),"×");
    }  
    if (resultat.charAt(resultat.length-1) == "/") {
        resultat = resultat.replace(resultat.charAt(resultat.length-1),"÷");
    }  
    document.getElementById("resultat").value = resultat;
    changeStateOperator(true);
}

function updateResultWithDot(value){
    resultat += value;
    calcul += value;
    
    document.getElementById("resultat").value = resultat;
    changeStateDot(true);
    changeStateOperator(true);
}

function showResult(){ 


    if (document.getElementById("resultat").value == "" && calcul == "") {
        document.getElementById("resultat").value = 0;
    } else {
        if (calcul == "") {
            resultat = document.getElementById("resultat").value;
            document.getElementById("resultat").value = eval(resultat);
        }else{
            if (calcul.charAt(0) == "*" || calcul.charAt(0) == "/") {
                calcul = calcul.replace(calcul.charAt(0),"");
                alert("expression mal formée mais l'opérateur au début du calcul a été supprimé automatiquement");
            }
            if (calcul.charAt(calcul.length-1) == "*" || calcul.charAt(calcul.length-1) == "/" || calcul.charAt(calcul.length-1) == "-" || calcul.charAt(calcul.length-1) == "+") {
                calcul = calcul.replace(calcul.charAt(resultat.length-1)," ");
                alert("expression mal formée mais l'opérateur a la fin du calcul a été supprimé automatiquement");
            }
            document.getElementById("resultat").value = eval(calcul);
        }        
    }
    resultat = ""; 
    calcul = ""; 
}

function resetResult(){
    resultat = "";
    calcul = "";
    document.getElementById("resultat").value = resultat;
}

function changeStateOperator(action){
    inputs[4].disabled = action;
    inputs[8].disabled = action;
    inputs[12].disabled = action;
    inputs[15].disabled = action;
}

function changeStateDot(action){
    inputs[14].disabled = action; 
}

function verifCaracteres(event){ 
	var touche = String.fromCharCode(event.keyCode);
	var champ = document.getElementById('resultat');
	var caracteres = '()/*-+0123456789';
    if(caracteres.indexOf(touche) >= 1) {
        champ.value += touche;
    }else{
        alert("Les caractères alphabétique sont interdits");
    }
}

```

## Problème

Je n'ai pas eu le temps de gérer tous les cas d'erreur possible comme par exemple le double operateur si l'utilisateur tape sans passer par les boutons.