var spanList = document.getElementsByTagName("span");
var startButton = document.getElementById("start");
var stopButton = document.getElementById("stop");
var t;
var milliseconde = 0;
var seconde = 0;
var minutes= 0;
var heure = 0;

function startTimer() {
  t = setInterval(update, 100);
  startButton.disabled = true;
}

function resetTimer() {
  clearInterval(t);
  startButton.disabled = false;
  (milliseconde = 0), (seconde = 0), (minutes= 0), (heure = 0);
  spanList[0].innerHTML=heure+" h";
  spanList[1].innerHTML=minutes+" min";
  spanList[2].innerHTML=seconde+" s";
  spanList[3].innerHTML=milliseconde+" ms"; 
}

function stopTimer() {
  clearInterval(t);
  startButton.disabled = false;
}

function update() {
  milliseconde += 1;
  if (milliseconde == 10) {
    milliseconde = 1;
    seconde += 1;
  }
  if (seconde == 60) {
    seconde = 0;
    minutes+= 1;
  }
  if (minutes== 60) {
    minutes= 0;
    heure += 1;
  }
  spanList[0].innerHTML=heure+" h";
  spanList[1].innerHTML=minutes+" min";
  spanList[2].innerHTML=seconde+" s";
  spanList[3].innerHTML=milliseconde+" ms"; 
}