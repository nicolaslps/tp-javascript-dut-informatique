# Compte rendu Javascript 



## Exercice 1 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body onload="checkCookie()">
 
<form action="" id="myForm">
    <label>Nom : </label>
    <input type="text" name="nom" id="nom"/>
    <input type="button" value="clicker" onclick="checkCookie(this.form.nom.value)" />
</form>

<div id="message">

</div>

<script src="./main.js"></script>
</body>
</html>
```

```javascript
function setCookie(cname,cvalue,exdays) {
    var date = new Date();
    date.setTime(date.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + date.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
  function checkCookie() {
    document.getElementById("message").innerHTML = "";
    var user=getCookie("userName");
    if (user != "") {
        document.getElementById("message").innerHTML = "Bienvenu de nouveau "+user;
    } else {
         user = prompt("Veuillez indiquer votre nom ");
         setCookie("userName", user, 30);
         document.getElementById("message").innerHTML = "Bienvenu "+ user;
    }
  }
```

## Exercice 2

```html
<!doctype html>
<html lang="en">
    <title>Racine somme vecteur</title>
</head>
<body>
  <form action="" id="myForm">
    <label>x : </label>
    <input type="text" name="x" id="x"/> <br>
    <label>n : </label>
    <input type="text" name="n" id="n"/> <br>
    <input type="button" value="calculer" onclick="calculer(this.form.x.value, this.form.n.value)" />
  </form>

  <div id="resultat">
    
  </div>
    <script src="./main.js"></script>
  </body>
</html>
```

```javascript
function calculer(x,n){
  var somme = 0;
  
  for (let i = 1; i <= n; i++) { 
    somme = somme + Math.pow(x,2);
  } 
  
  var resultat = Math.sqrt(somme);
  document.getElementById("resultat").innerHTML = resultat;
}
```

## Exercice 3

```html
<!doctype html>
<html lang="en">
    <title>Racine somme vecteur</title>
</head>
<body>
  <form action="" id="myForm">
    <label>Veuillez entrer un vecteur à compresser avec comme séparateur "," : </label>
    <input type="text" name="vecteur" id="vecteur"/> <br>
    <input type="button" value="calculer" onclick="zip(this.form.vecteur.value.split(','))" />
  </form>

  <div id="resultat">
    
  </div>
    <script src="./main.js"></script>
  </body>
</html>
```

```javascript
var i=0;
var listeAncienComposantVecteur=[];
var vecteurComporesse=[];


function zip(v){
	var push = true;
	for(var j=0; j<=listeAncienComposantVecteur.length; j++){
		if(v[i] == listeAncienComposantVecteur[j]){
			push = false;
		}
	}

	if(push){
		vecteurComporesse.push(v[i]);
		listeAncienComposantVecteur.push(v[i]);
	}
	
	if (i < v.length){
		i++;
		return zip(v);
	} else {
		document.getElementById("resultat").innerHTML = vecteurComporesse;
	}
}
```

## Exercice 4

```html
<!doctype html>
<html lang="en">
    <title>exo 4</title>
</head>
<body>
  <form action="" id="myForm">
    <label>n : </label>
    <input type="text" name="n" id="n"/> <br>
    <input type="button" value="calculer la somme des n premiers carrés" onclick="somme_carres(this.form.n.value)" /> <br>
    <input type="button" value="calculer la somme des n premiers carrés (récursive)" onclick="somme_carres_recusrive(this.form.n.value, 0)" />
  </form>

  <div id="resultat">
    
  </div>
  <div id="resultat2">
    
  </div>
    <script src="./main.js"></script>
  </body>
</html>
```

```javascript
function somme_carres(n){
    var somme = 0;

    for (let i = 0; i <= n; i++) {
        somme = somme + Math.pow(i,2);        
    }

    document.getElementById("resultat").innerHTML = "Résultat : "+somme;
}

function somme_carres_recusrive(n, somme){
    console.log(n);
    console.log(somme);
    var sommeRecursive = somme;
    if (n == 0) {
        document.getElementById("resultat2").innerHTML =  "Résultat : "+sommeRecursive;
    } else {
        
        sommeRecursive = sommeRecursive + Math.pow(n,2);
        n--; 
        console.log(sommeRecursive);
        return somme_carres_recusrive(n, sommeRecursive);
    }  
}

```


## Exercice 5

```html
<!doctype html>
<html lang="en">
    <title>exo 5</title>
</head>
<body>
  <form action="" id="myForm">
    <label>Ecribre un texte</label>
    <input type="text" name="texte" id="texte"/> <br>
    <label>premier terme</label>
    <input type="text" name="premier" id="premier"/> <br>
    <label>second terme</label>
    <input type="text" name="second" id="second"/> <br>
    <input type="button" value="calculer" onclick="sequence(this.form.texte.value, this.form.premier.value, this.form.second.value)" />
  </form>
    <script src="./main.js"></script>
  </body>
</html>
```

```javascript
function sequence(chaine,a,b){
    positionA = chaine.indexOf(a);
    positionB = chaine.indexOf(b);

    if (positionA < positionB) {
        alert("return true");
        return true;
    } else {
        alert("return false");
        return false;
    }
}
```



## Problème

De façon globale je n'ai pas eu de problème au niveau du code car dès qu'il y avais un problème en allant sur w3schools ou stackoverflow je trouvais des éléments pour me débloquer.

En revanche sur la formulation des questions j'ai eu des problème de compréhension par exemple l'exercice 2 j'ai coder une fonction qui calcul la formule et non la phrase étant donné que les deux ne décrivent pas la même chose.

Ensuite pour la fonction récursive de somme-carres(n) j'ai ajouté un paramètre pour permettre le retour à 0 lorsque l'on utilise de nouveau la fonction mais je ne suis pas sur que ce soit ce qu'il faillais faire. 

Hormis cela aucun soucis à déclarer !