function setCookie(cname,cvalue,exdays) {
    var date = new Date();
    date.setTime(date.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + date.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
  function checkCookie() {
    document.getElementById("message").innerHTML = "";
    var user=getCookie("userName");
    if (user != "") {
        document.getElementById("message").innerHTML = "Bienvenu de nouveau "+user;
    } else {
         user = prompt("Veuillez indiquer votre nom ");
         setCookie("userName", user, 30);
         document.getElementById("message").innerHTML = "Bienvenu "+ user;
    }
  }

 