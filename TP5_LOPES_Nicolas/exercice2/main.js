setUpMap();
var map;

function setUpMap(){
map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([37.41, 8.82]),
      zoom: 4
    })
  });
}
 
var lat = 48.866667;
var long = 2.333333;
function CenterMap() {
    map.getView().setCenter(ol.proj.fromLonLat([long, lat]));
}